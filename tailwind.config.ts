import type { Config } from 'tailwindcss';

const config: Config = {
	content: [
		'./src/pages/**/*.{js,ts,jsx,tsx,mdx}',
		'./src/components/**/*.{js,ts,jsx,tsx,mdx}',
		'./src/app/**/*.{js,ts,jsx,tsx,mdx}',
	],
	theme: {
		extend: {
			colors: {
				black: '#0f0f0f',
				purple: '#883ff9',
				// pink: '#21b5ff',
				pink: '#ec167f',
				// purple: '#1680ff',
			},
			transitionProperty: {
				height: 'height',
			},
			variants: {
				height: ['responsive', 'hover', 'focus'],
			},
		},
	},
	plugins: [],
};

export default config;
