'use client';
import { useScrollPosition } from '@/hooks';
import clsx from 'clsx';
import React from 'react';
import { icons } from '../../../public/icons';
import { Anchor, Button, Logo, Parapraph, Title } from '../common';

const items = [
	{ label: 'Accueil', key: '#' },
	{ label: 'A propos', key: '#about' },
	{ label: 'Speakers', key: '#speaker' },
	{ label: 'Planning', key: '#schedule' },
	{ label: 'Tournoi', key: '#tournament' },
	{ label: 'Ticket', key: '#ticket' },
	{ label: 'Partenaires', key: '#sponsor' },
	{ label: 'Contact', key: '#contact' },
];

export const Nav = () => {
	const [isOpen, setIsOpen] = React.useState(false);

	const scrollPosition = useScrollPosition();
	return (
		<>
			<nav
				className={clsx({
					'w-screen fixed top-0 z-50 transition-all duration-500': true,
					'bg-black/100': scrollPosition > 0,
					'bg-black/0': scrollPosition === 0,
				})}
			>
				<div className="flex items-center justify-center lg:justify-between content lg:max-w-screen-xl py-5">
					<Logo data-aos="fade-right" data-aos-delay="100" />
					<div className="flex items-center gap-10">
						<ul
							className="hidden lg:flex items-center gap-5"
							data-aos="fade-down"
							data-aos-delay="200"
						>
							{items.map(({ key, label }, index) => (
								<React.Fragment key={key}>
									<li>
										<Anchor
											href={key}
											className="text-white uppercase hover:text-pink transition-all font-bold text-sm"
										>
											{label}
										</Anchor>
									</li>
									{items.length - 1 === index ? (
										<></>
									) : (
										<div className="h-[4px] w-[4px] bg-pink rounded-sm relative flex items-center" />
									)}
								</React.Fragment>
							))}
						</ul>
						
					</div>
				</div>
			</nav>
			<div
				className={clsx({
					'opacity-100 pointer-events-auto': isOpen,
					'opacity-0 pointer-events-none': !isOpen,
					'hidden lg:flex transition-all bg-black/30 h-screen w-screen fixed top-0 left-0 z-50':
						true,
				})}
			>
				<div
					className={clsx({
						'hidden min-h-screen lg:flex flex-col bg-[#0b0b0b] w-1/4': true,
						'absolute right-0 top-0 z-50 px-14 py-16 gap-10': true,
					})}
				>
					<div className="grid gap-2">
						<div className="flex items-center justify-between">
							<Title value="Information pratiques" className="text-xl font-bold" />
							<button onClick={() => setIsOpen(false)} className="bg-none text-pink h-8 w-8">
								{icons.close}
							</button>
						</div>
						<Parapraph value="" />
					</div>

					<div className="border-[1px] border-white p-8 text-center flex flex-col items-center gap-8">
						<div>
							<Title value="485 of 1000" className="font-bold text-2xl" />
							<Parapraph value="Seats available" />
						</div>

						<Button label="Get your ticket" />
					</div>

					<div className="grid gap-4">
						<Title value="Where & When ?" className="text-xl font-bold" />
						<div className="grid gap-2 font-bold">
							<div className="flex items-center gap-2">
								<div className="text-pink h-5 w-5">{icons.calendar}</div>
								<Parapraph value="June 20th to 25th" />
							</div>
							<div className="flex items-center gap-2">
								<div className="text-pink h-5 w-5">{icons.location}</div>
								<Parapraph value="Palo Alto, California" />
							</div>
							<div className="flex items-center gap-2">
								<div className="text-pink h-5 w-5">{icons.phone}</div>
								<Parapraph value=" 200 300 9000" />
							</div>
							<div className="flex items-center gap-2">
								<div className="text-pink h-5 w-5">{icons.email}</div>
								<Parapraph value="info@exhibiztheme.com" />
							</div>
						</div>
					</div>
				</div>
			</div>
		</>
	);
};
