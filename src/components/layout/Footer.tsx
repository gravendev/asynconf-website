import { LinkIcon, Logo, Parapraph } from '..';

export const Footer = () => {
	return (
		<footer className="content lg:max-w-screen-xl py-20 gap-10 flex flex-col lg:flex-row items-center justify-between relative">
			<Logo data-aos="fade-up" data-aos-delay="100" />
			<Parapraph
				data-aos="fade-up"
				data-aos-delay="200"
				value="© Copyright 2023 - Asynconf Evenement"
				className="left-0 right-0 mx-auto lg:absolute text-center pointer-events-none"
			/>
			<div className="flex items-center gap-2" data-aos="fade-up" data-aos-delay="300">
				<LinkIcon name="linkedin" href='https://www.linkedin.com/company/asynconf/' />
				<LinkIcon name="twitter" href='https://twitter.com/AsynconfEvent' />
				<LinkIcon name="instagram" href='https://www.instagram.com/asynconf/' />
				<LinkIcon name="discord" href='https://discord.gg/StQCRRWUDc' />

			</div>
		</footer>
	);
};
