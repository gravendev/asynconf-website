import NextLink from 'next/link';
import type { ComponentProps, ReactElement } from 'react';
import { forwardRef } from 'react';

type AnchorProps = Omit<ComponentProps<'a'>, 'ref'> & {
	newWindow?: boolean;
	prefetch?: boolean;
};

// eslint-disable-next-line react/display-name
export const Anchor = forwardRef<HTMLAnchorElement, AnchorProps>(function (
	{ href = '', children, newWindow, prefetch = true, ...props },
	// ref is used in <NavbarMenu />
	forwardedRef
): ReactElement {
	if (newWindow) {
		return (
			<a href={href} ref={forwardedRef} rel="noreferrer" target="_blank" {...props}>
				{children}
			</a>
		);
	}

	if (!href) {
		return (
			<a ref={forwardedRef} {...props}>
				{children}
			</a>
		);
	}

	if (href.includes('#')) {
		return (
			<a href={href} ref={forwardedRef} {...props}>
				{children}
			</a>
		);
	}

	return (
		<NextLink href={href} prefetch={prefetch} ref={forwardedRef} {...props}>
			{children}
		</NextLink>
	);
});
