export const Parapraph: React.FC<{ value: string; className?: string }> = ({
	value,
	className,
	...props
}) => (
	<p className={`${className} text-white/50 text-sm lg:text-base`} {...props}>
		{value}
	</p>
);
