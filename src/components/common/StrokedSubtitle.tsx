export const StrokedSubtitle: React.FC<{ value?: string }> = ({ value, ...props }) =>
	value ? (
		<p
			className="font-bold absolute text-[280px] font-stroke mx-auto z-0 pointer-events-none"
			{...props}
		>
			{value}
		</p>
	) : (
		<></>
	);
