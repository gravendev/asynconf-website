import clsx from 'clsx';

export const Divider = () => (
	<div
		className={clsx({
			'h-2 w-2 bg-pink rounded-sm relative flex items-center': true,
			"before:content-[''] before:w-[300px] before:h-[1px]": true,
			'before:bg-white/10 before:absolute before:right-5': true,
			"after:content-[''] after:w-[300px] after:h-[1px]": true,
			'after:bg-white/10 after:absolute after:left-5': true,
		})}
	/>
);
