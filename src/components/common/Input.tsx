interface IProps {
	onChange?: (value: React.FormEvent<HTMLInputElement>) => any;
	type?: 'password' | 'email';
	placeholder: string;
	value?: string;
	id?: string;
}

export const Input: React.FC<IProps> = ({ id, placeholder, value, onChange, type }) => (
	<input
		className="bg-[#0b0b0b] placeholder-white/50 text-white py-4 px-4 outline-pink focus:outline"
		placeholder={placeholder}
		onChange={onChange}
		value={value}
		type={type}
		id={id}
		required
	/>
);
