import { Image } from '.';

export const Logo = ({ ...props }) => (
	<div className="flex items-center" {...props}>
		<Image src="/logo.svg" alt="logo" className="h-10 w-16 mr-2 relative" objectFit="contain" />
	</div>
);
