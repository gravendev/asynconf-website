import clsx from 'clsx';

interface IProps {
	icon?: any;
	label?: string;
	className?: string;
	onClick?: () => any;
	isOutlined?: boolean;
	type?: 'submit' | 'reset' | 'button';
}

export const Button: React.FC<IProps> = ({
	icon,
	label,
	onClick,
	isOutlined,
	type,
	className,
	...props
}) => (
	<button
		onClick={onClick}
		type={type || 'button'}
		className={clsx({
			'py-2 px-4': !icon,
			[className || '']: !!className,
			'hover:bg-pink hover:border-black hover:text-black': isOutlined,
			'tracking-widest uppercase text-white font-light text-sm': true,
			'rounded-sm cursor-pointer w-fit transition-all': true,
			'bg-black border-[1px] border-white/30': isOutlined,
			'bg-pink hover:opacity-75': !isOutlined,
		})}
		{...props}
	>
		{icon || label}
	</button>
);
