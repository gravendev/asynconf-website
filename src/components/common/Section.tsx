import clsx from 'clsx';
import { Divider, StrokedSubtitle, Title } from '.';

interface IProps {
	id?: string;
	title: string;
	subtitle: string;
	classNames?: string;
	withDivider?: boolean;
	children?: React.ReactNode;
}

export const Section: React.FC<IProps> = ({
	id,
	title,
	subtitle,
	children,
	classNames,
	withDivider,
}) => {
	return (
		<section id={id} className={`relative bg-no-repeat bg-center bg-cover bg-fixed ${classNames}`}>
			<div className="py-40 flex flex-col gap-10 items-center relative content lg:max-w-screen-xl">
				<div className="relative flex items-center justify-center">
					<StrokedSubtitle value={subtitle} data-aos="fade-up" data-aos-delay="200" />
					<Title value={title} data-aos="fade-up" data-aos-delay="300" />
				</div>
				{withDivider ? <Divider /> : <></>}
				<div className="w-full flex justify-center" data-aos="fade-up" data-aos-delay="400">
					{children}
				</div>
			</div>
		</section>
	);
};
