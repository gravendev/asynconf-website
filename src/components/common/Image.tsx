import NextImage from 'next/image';

interface IProps {
	src: string;
	alt: string;
	className?: string;
	objectFit?: 'contain' | 'cover';
}

export const Image: React.FC<IProps> = ({ src, alt, className, objectFit, ...props }) => (
	<div className={className || 'relative'} {...props}>
		<NextImage fill style={{ objectFit: objectFit || 'cover' }} src={src} alt={alt} />
	</div>
);
