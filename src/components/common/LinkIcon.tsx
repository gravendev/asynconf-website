import React from 'react';
import { icons } from '../../../public/icons';

export const LinkIcon: React.FC<{ name: string; href?: string }> = ({ name, href }) => {
	// @ts-ignore
	const icon = React.useMemo(() => icons[name], [name]);
	return (
		<a href={href || '#'} className="hover:text-pink cursor-pointer transition-colors">
			{icon}
		</a>
	);
};
