export const Title: React.FC<{ value?: string; className?: string }> = ({
	value,
	className,
	...props
}) =>
	value ? (
		<h2 className={className || 'text-4xl font-bold'} {...props}>
			{value}
		</h2>
	) : (
		<></>
	);
