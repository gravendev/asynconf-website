import { Button } from '..';

export const RegistrationSection = () => (
	<section id='ticket' className="py-24 bg-gradient-to-r from-pink to-purple">
		<div className="content lg:max-w-screen-xl flex flex-col lg:flex-row items-end lg:items-center lg:justify-between">
			<p data-aos="fade-right" data-aos-delay="200" className="text-2xl font-bold">
				{`Inscrivez-vous gratuitement à l'évenement`}
			</p>
			<Button label="Soon" data-aos="fade-left" data-aos-delay="300" />
		</div>
	</section>
);
