'use client';
import { Button, Input } from '../common';
import emailjs from '@emailjs/browser';

export const ContactForm = () => {
	const onSubmit = (event: any) => {
		event.preventDefault();

		const target = event.target;

		const data = {
			name: target.name.value,
			email: target.email.value,
			subject: target.subject.value,
			message: target.message.value,
		};

		if (data.email != null && data.message != null){
			emailjs.send(
				'service_1505y6m',
				'template_4512y6h',
				{
					subject: data.subject,
					email: data.email,
					message: data.message
				},
				'6okWBTcAnGa_0b5Fw'
			)
			alert("Mail envoyé")
		}
	};

	return (
		<form
			className="py-5 w-full lg:max-w-screen-lg flex flex-col items-center gap-10 relative"
			onSubmit={onSubmit}
		>
			<div className="grid lg:grid-cols-2 gap-10 w-full">
				<div className="grid gap-5">
					<Input id="name" placeholder="Votre nom" />
					<Input id="email" type="email" placeholder="Votre email" />
					<Input id="subject" placeholder="Votre Sujet" />
				</div>
				<textarea
					className="bg-[#0b0b0b] min-h-[178px] placeholder-white/50 text-white py-4 px-4 outline-pink focus:outline"
					placeholder="Votre message"
					id="message"
				/>
			</div>
			<Button type="submit" label="Envoyer" isOutlined />
		</form>
	);
};
