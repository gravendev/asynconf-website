import { Button, Image, Parapraph, StrokedSubtitle, Title } from '..';
import { icons } from '../../../public/icons';

export const TournamentSection = () => {
	return (
		<section id="tournament" className="py-20 bg-no-repeat bg-center bg-cover bg-fixed bg-welcome">
			<div className="py-10 content lg:max-w-screen-xl relative flex flex-col lg:flex-row gap-y-20 lg:gap-x-20 items-center">
				<div className="-top-40 absolute">
					<StrokedSubtitle value="hackathon" data-aos="fade-up" data-aos-delay="100" />
				</div>
				<div className="flex flex-col gap-8 w-full lg:w-1/2">
					<h1 data-aos="fade-up" data-aos-delay="300" className="text-2xl lg:text-4xl font-bold">
					Tournoi de programmation
					</h1>
					<Parapraph
						data-aos="fade-up"
						data-aos-delay="400"
						value={`La Banque Populaire se lance dans l'exploration des territoires de l'innovation et du numérique. Grâce à cette initiative, nous sommes en train d'organiser un tournoi de code axé sur le thème de la "Banque de demain". C'est une formidable occasion pour vous de relever le défi et de vous mesurer à d'autres participants sur un projet passionnant. `}
					/>
					<a href='https://bit.ly/46fCy7E'><img
						alt="illustration"
						width={400}
						src="/images/sponsors/white-td.png"
					/></a>
					
				</div>
				<div className="grid w-full lg:w-1/2">
						<div className="bg-black">

							<div className="py-5 px-4 bg-white/10 flex items-center gap-2">
								<span className="text-pink h-6 w-6">{icons.check}</span>
								<p className="text-sm">Début Lundi 30/10 à 12H30</p>
								
							</div>
							<div className="py-5 px-4 bg-white/10 flex items-center gap-2">
								<span className="text-pink h-6 w-6">{icons.check}</span>
								<p className="text-sm">24H pour faire le projet</p>
								
							</div>
							<div className="py-5 px-4 bg-white/10 flex items-center gap-2">
								<span className="text-pink h-6 w-6">{icons.check}</span>
								<p className="text-sm">Aucune inscription requise</p>
								
							</div>
							<div className="p-10 flex items-center justify-center">
								<a href="/docs/reglement_tournoi.pdf" target='_blank' className=' bg-purple text-center w-8 p-2 tracking-widest uppercase text-white font-light text-sm rounded-sm cursor-pointer w-fit transition-all hover:opacity-75 mr-10'>
									Reglement de Tournoi
								</a>
								<a href="/docs/sujet_tournoi.pdf" target='_blank' className='  bg-purple text-center w-8 p-2 tracking-widest uppercase text-white font-light text-sm rounded-sm cursor-pointer w-fit transition-all hover:opacity-75 mr-10'>
									Enoncé du tournoi
								</a>
								<a href="https://docs.google.com/forms/d/1W9MxiBZYAD86Re6PTzZldBbZ0-4_39-z5nngfBcDe0M/" target='_blank' className='bg-purple  text-center w-8 p-2 tracking-widest uppercase text-white font-light text-sm rounded-sm cursor-pointer w-fit transition-all hover:opacity-75'>
									Soumettre le projet
								</a>

							</div>
							
							<div className="h-1 bg-gradient-to-r from-pink to-purple" />
						</div>
					
				</div>
			</div>
		</section>
	);
};
