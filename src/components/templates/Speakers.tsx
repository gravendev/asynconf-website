import { speakers } from '@/data';
import clsx from 'clsx';
import { Image, LinkIcon } from '..';

export const Speakers = () => {
	return speakers.length > 0 ? (
		<div className="gap-10 grid-cols-1 grid lg:grid-cols-4 w-full">
			{speakers.map((speaker, index) => (
				<div
					key={index}
					className="group h-[276px] cursor-pointer relative rounded-md overflow-hidden"
				>
					<Image src={speaker.picture} alt={speaker.name} className="absolute h-[276px] w-full" />

					<div
						className={clsx({
							'bg-black w-full p-10 h-0 grid gap-6 opacity-0': true,
							'absolute z-30 group-hover:h-full group-hover:opacity-100': true,
							'transition-all duration-500 ease-in-out': true,
						})}
					>
						<div>
							<p className="font-bold text-xl">{speaker.name}</p>
							<p className="text-pink font-light">{speaker.role}</p>
						</div>
						<div className="bg-white/50 h-[1px] w-1/4" />
						<p className="text-sm">{speaker.description}</p>
						<div className="flex items-center gap-4">
							<LinkIcon name="linkedin" href={speaker.linkedin} />
							<LinkIcon name="twitter" href={speaker.twitter} />
						</div>
					</div>
				</div>
			))}
		</div>
	) : (
		<></>
	);
};
