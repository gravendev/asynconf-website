import { features } from '@/data';
import { Parapraph } from '..';
import { icons } from '../../../public/icons';

export const Features = () => (
	<div className="grid grid-cols-1 gap-10 lg:grid-cols-3 relative">
		{features.map((feature) => {
			// @ts-ignore
			const icon = icons[feature.icon];
			return (
				<div key={feature.id} className="flex gap-x-5">
					<div className="flex items-center justify-center h-20 w-20 bg-pink rounded-sm p-5 hover:bg-purple transition-all duration-300">
						{icon}
					</div>
					<div className="flex flex-col gap-2 flex-1">
						<h3 className="font-bold text-xl">{feature.title}</h3>
						<Parapraph value={feature.description} />
					</div>
				</div>
			);
		})}
	</div>
);
