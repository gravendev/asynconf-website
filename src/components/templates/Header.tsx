'use client';
import 'swiper/css';
import 'swiper/css/effect-fade';
import { Autoplay, EffectFade } from 'swiper/modules';
import { Swiper, SwiperSlide } from 'swiper/react';
import { icons } from '../../../public/icons';
import { Anchor, Button, Image } from '../common';

export const Header = () => {
	return (
		<header className="relative">
			<div className="absolute h-full w-full z-0" data-aos="zoom-in" data-aos-delay="200">
				<Swiper
					autoplay
					effect="fade"
					navigation={true}
					slidesPerView={1}
					modules={[EffectFade, Autoplay]}
				>
					<SwiperSlide>
						<Image
							alt="illustration"
							src="/images/slider/wide1.jpg"
							className="min-h-[75vh] lg:min-h-[76vh] w-screen"
						/>
					</SwiperSlide>
					<SwiperSlide>
						<Image
							alt="illustration"
							src="/images/slider/wide2.jpg"
							className="min-h-[75vh] lg:min-h-[76vh] w-screen"
						/>
					</SwiperSlide>
				</Swiper>
			</div>
			<div className="content lg:max-w-screen-lg text-center min-h-[50vh] lg:min-h-[75vh] py-20 flex items-center justify-center flex-col">
				<h1
					data-aos="fade-up"
					data-aos-delay="100"
					className="lg:mt-auto tracking-tighter font-bold text-[44px] leading-[32px] lg:text-[120px] lg:leading-[100px]"
				>
					ASYNCONF
					<br />
					Expo 2023
				</h1>

				<div className="lg:flex items-center gap-12 mt-auto justify-center">
					<div data-aos="fade-up" data-aos-delay="200" className="flex items-center gap-[12px] md:block hidden">
						<div className='items-center flex gap-[12px] '>
							<div className="h-6 w-6 text-pink">{icons.calendar}</div>
							<p className="font-light">30-31 octobre 2023</p>
						</div>
					</div>
					<div className="z-10 h-fit" data-aos="fade-up" data-aos-delay="100">
						<Anchor href="https://www.youtube.com/watch?v=dYykvBe04rQ">
						<Button label="Regarder le direct" className='w-48 h-16 text-sm' />
						</Anchor>
					</div>
					<div data-aos="fade-up" data-aos-delay="300" className="flex items-center gap-[12px] hidden md:block">
						<div className='items-center flex gap-[12px] '>
							<div className="h-6 w-6 text-pink">{icons.location}</div>
							<p className="font-light">Paris Saclay + En Ligne</p>
						</div>
					</div>
					</div>
				</div>
		</header>
	);
};
