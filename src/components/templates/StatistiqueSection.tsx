import { statistiques } from '@/data';
import { icons } from '../../../public/icons';

export const StatistiqueSection = () => (
	<section className="bg-stats bg-no-repeat bg-top bg-cover bg-fixed">
		<div className="py-32 content lg:max-w-screen-lg flex flex-col lg:flex-row gap-16 lg:gap-10 justify-center lg:justify-between lg:items-start">
			{statistiques.map((stat, index) => {
				// @ts-ignore
				const icon = icons[stat.icon];
				return (
					<div
						key={stat.id}
						data-aos="fade-up"
						data-aos-delay={(index + 1) * 100}
						className="flex flex-col gap-5 text-center items-center"
					>
						<div className="h-16 w-16 text-white">{icon}</div>
						<p className="font-bold text-6xl">{stat.value}</p>
						<p className="font-light uppercase text-lg">{stat.label}</p>
					</div>
				);
			})}
		</div>
	</section>
);
