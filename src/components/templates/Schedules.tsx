'use client';
import { schedule } from '@/data';
import clsx from 'clsx';
import React from 'react';
import { Image, Parapraph } from '../common';

export const Schedule = () => {
	const [activeDay, setActiveDay] = React.useState(1);

	const selectedProgram = React.useMemo(
		() => schedule.find((day) => day.id === activeDay)?.program.sort(),
		[activeDay, schedule]
	);

	return (
		<section>
			<div className="grid lg:grid-cols-2 lg:w-1/3 mx-auto">
				{schedule.map((day, index) => {
					const isSelected = day.id === activeDay;
					return (
						<div
							onClick={() => setActiveDay(day.id)}
							key={day.id}
							className={clsx({
								'transition-all duration-500': true,
								'bg-pink text-white': isSelected,
								'px-6 py-4 text-center cursor-pointer': true,
								'bg-white/10 text-white/50 lg:hover:bg-pink lg:hover:text-white': !isSelected,
								'rounded-tl-md rounded-tr-md lg:rounded-l-md lg:rounded-tr-none': index === 0,
								'rounded-bl-md rounded-br-md lg:rounded-r-md lg:rounded-bl-none': index === 1,
							})}
						>
							<p className="font-bold text-white text-2xl">{day.label}</p>
							<p className="font-bold text-sm">{day.date}</p>
						</div>
					);
				})}
			</div>
			<div>
				{selectedProgram && selectedProgram.length > 0 ? (
					selectedProgram.map(({ id, hour, speaker, description, name }) => (
						<div
							key={id}
							className="py-10 flex lg:items-center flex-col lg:flex-row gap-5 lg:gap-20 border-b-[1px] border-b-white/10"
						>
							<Parapraph className="font-bold" value={hour} />
							<div className="flex items-center gap-5 group">
								<div className="bg-black group-hover:bg-pink p-[5px] rounded-full duration-500 ease-in-out transition-all">
									<Image
										className="h-20 w-20 relative rounded-full overflow-hidden"
										src={speaker.picture}
										alt={speaker.name}
									/>
								</div>
								<div>
									<p className="font-bold text-base">{speaker.name}</p>
									<p className="text-pink font-light text-sm">{speaker.role}</p>
								</div>
							</div>
							<div className="lg:w-1/2 gap-4 grid">
								<p className="font-bold text-2xl">{name}</p>
								<Parapraph value={description} />
							</div>
						</div>
					))
				) : (
					<></>
				)}
			</div>
		</section>
	);
};
