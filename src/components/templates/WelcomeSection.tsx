import { Image, Parapraph, StrokedSubtitle } from '..';

export const WelcomeSection = () => {
	return (
		<section id="about" className="py-20 bg-no-repeat bg-center bg-cover bg-fixed bg-welcome">
			<div className="py-10 content lg:max-w-screen-xl relative flex flex-col lg:flex-row gap-y-20 lg:gap-x-20 items-center">
				<div className="-top-40 absolute">
					<StrokedSubtitle value="welcome" data-aos="fade-up" data-aos-delay="100" />
				</div>
				<div className="flex flex-col gap-8 w-full lg:w-1/2">
					<h1 data-aos="fade-up" data-aos-delay="300" className="text-2xl lg:text-4xl font-bold">
						Bienvenue sur le site officiel du salon AsynConf 2023 !
					</h1>
					<Parapraph
						data-aos="fade-up"
						data-aos-delay="400"
						value={`Nous sommes ravis de vous accueillir à cet événement incontournable dédié aux devs et aux métiers de l'informatique. Après avoir été organisé à distance, nous sommes heureux de vous annoncer que cette 4e édition se tiendra également en physique les 30 et 31 octobre 2023 à Paris Saclay.`}
					/>
					<Parapraph
						data-aos="fade-up"
						data-aos-delay="500"
						value={`Rejoignez-nous pour assister à des conférences captivantes, rencontrer des experts renommés et élargir vos connaissances dans ces domaines en constante évolution. Ne manquez pas cette occasion unique de vous connecter, d'apprendre et d'échanger avec les meilleurs esprits de l'industrie.`}
					/>
					<Parapraph
						data-aos="fade-up"
						data-aos-delay="600"
						value={`Cet évenement est propulsé par le youtubeur Graven Développement et la Terrasse Discovery - Banque Populaire Val de France)`}
					/>

					<a href='https://bit.ly/46fCy7E'><img
						alt="illustration"
						width={400}
						src="/images/sponsors/white-td.png"
					/></a>
					
				</div>
				<div className="relative w-full lg:w-1/2">
					<Image
						alt="illustration"
						src="/images/welcome/1.jpg"
						data-aos="fade-left"
						data-aos-delay="200"
						className="h-[235px] lg:h-[384px] w-full relative z-0"
					/>
					<Image
						alt="illustration"
						src="/images/welcome/2.jpg"
						data-aos="fade-up"
						data-aos-delay="300"
						className="h-[120px] w-[180px] lg:h-[190px] lg:w-[280px] absolute -bottom-20 lg:-bottom-24 -left-10 -z-10"
					/>
					<Image
						alt="illustration"
						src="/images/welcome/3.jpg"
						data-aos="fade-left"
						data-aos-delay="400"
						className="h-[100px] w-[150px] lg:h-[150px] lg:w-[230px] absolute -bottom-10 -right-10"
					/>
					
				</div>
			</div>
		</section>
	);
};
