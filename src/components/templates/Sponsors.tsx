import { sponsors } from '@/data';
import Image from 'next/image';
import Link from 'next/link';

export const Sponsors = () =>
	sponsors.length > 0 ? (
		<div className="grid lg:grid-cols-4 gap-10 items-center justify-center py-20">
			{sponsors.map((sponsor) => (
				<Link key={sponsor.id} href={sponsor.link}>
					<Image
						height={100}
						width={300}
						alt="sponsor"
						src={sponsor.picture}
						style={{ objectFit: 'contain' }}
					/>
				</Link>
			))}
		</div>
	) : (
		<></>
	);
