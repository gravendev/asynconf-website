import { tickets } from '@/data';
import { Button } from '..';
import { icons } from '../../../public/icons';
import Link from 'next/link';

export const Tickets = () => (

	
	<div className="grid gap-20 grid-cols-1 lg:grid-cols-3 w-full">
		{tickets.map((ticket) => (
			<div key={ticket.id} className="bg-black">
				<div className="p-10 text-center flex flex-col items-center justify-center gap-10">
					<p className="text-2xl">{ticket.title}</p>
					<p className="text-5xl font-bold">{ticket.price}</p>
				</div>
				<div className="py-5 px-4 bg-white/10 flex items-center gap-2">
					<span className="text-pink h-6 w-6">{icons.check}</span>
					<p className="text-sm">{ticket.description}</p>
				</div>
				<div className="p-10 flex items-center justify-center">
					<a href={ticket.link} target='_blank' className='h-8 w-8 p-2 tracking-widest uppercase text-white font-light text-sm rounded-sm cursor-pointer w-fit transition-all bg-pink hover:opacity-75'>{ticket.button}</a>
				</div>
				<div className="h-1 bg-gradient-to-r from-pink to-purple" />
			</div>
		))}
	</div>
);
