import { Image } from '..';

export const Portfolio = () => {
	return (
		<section className="grid lg:grid-cols-4 w-screen bg-pink">
			{[1, 2, 3, 4].map((item) => (
				<div key={item} className="h-[320px] cursor-pointer lg:h-[218px] overflow-hidden group">
					<Image
						key={item}
						alt="image"
						src={`/images/portfolio/${item}.jpg`}
						className="h-full w-full relative  group-hover:scale-150 transition-all duration-500"
					/>
				</div>
			))}
		</section>
	);
};
