'use client';
import { useCountdown } from '@/hooks';
import { useState, useEffect } from 'react';

const items = [
	{ key: 'days', label: 'Jours' },
	{ key: 'hours', label: 'Heures' },
	{ key: 'minutes', label: 'Minutes' },
	{ key: 'seconds', label: 'Secondes' },
];

export const CountdownSection = () => {
	const [isClient, setIsClient] = useState(false);

	useEffect(() => {
		setIsClient(true);
	}, []);

	const countdown = useCountdown(new Date('2023-10-30T09:00:00'));
	return isClient ? (
		<section className="py-12 bg-gradient-to-r from-pink to-purple">
			<div className="content lg:max-w-screen-md grid grid-cols-4 gap-10">
				{items.map((item, index) => {
					// @ts-ignore
					const value = countdown[item.key];
					return (
						<div
							key={item.key}
							data-aos="fade-up"
							data-aos-duration={index + 1 * 100}
							className="flex flex-col items-center justify-between"
						>
							<p className="text-4xl lg:text-7xl">{value}</p>
							<p className="text-white/70 uppercase text-light text-sm lg:text-lg">{item.label}</p>
						</div>
					);
				})}
			</div>
		</section>
	) : (
		<></>
	);
};
