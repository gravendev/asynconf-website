import {
	ContactForm,
	CountdownSection,
	Features,
	Header,
	Nav,
	Portfolio,
	Schedule,
	Section,
	Speakers,
	Sponsors,
	StatistiqueSection,
	Tickets,
	WelcomeSection,
	TournamentSection,
} from '@/components';

import Script from 'next/script';

export default function Home() {
	return (
		<main className="w-screen overflow-x-hidden relative">
			<Header />
			<WelcomeSection />
			<Section id="feature" title="A découvrir au salon" subtitle="features" withDivider>
				<Features />
			</Section>
			<Section
				withDivider
				id="speaker"
				subtitle="speakers"
				title="Speakers 2023"
				classNames="bg-speakers"
			>
				<Speakers />
			</Section>
			<Section
				withDivider
				id="schedule"
				subtitle="schedule"
				classNames="bg-schedules"
				title="Planning du salon"
			>	
				<Schedule />
			</Section>
			<Section
				withDivider
				id="ticket"
				subtitle="tickets"
				classNames="bg-ticket"
				title="Choisir un ticket"
			>
				<Tickets />
			</Section>
			
			<TournamentSection />
			
			<Section
				withDivider
				id="sponsor"
				title="Partenaires"
				subtitle="sponsors"
				classNames="bg-sponsors"
			>
				<Sponsors />
			</Section>
			<Portfolio />
			<Section withDivider id="contact" title="Contactez-nous" subtitle="contact">
				<ContactForm />
			</Section>
			<StatistiqueSection />
			
			<Script id="gtm-script" strategy="lazyOnload">
				{`
							var _paq = window._paq = window._paq || [];
							/* tracker methods like "setCustomDimension" should be called before "trackPageView" */
							_paq.push(['trackPageView']);
							_paq.push(['enableLinkTracking']);
							(function() {
							  var u="https://asynconf.matomo.cloud/";
							  _paq.push(['setTrackerUrl', u+'matomo.php']);
							  _paq.push(['setSiteId', '1']);
							  var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
							  g.async=true; g.src='//cdn.matomo.cloud/asynconf.matomo.cloud/matomo.js'; s.parentNode.insertBefore(g,s);
							})();
				`}
			</Script>

		</main>
	);
}
