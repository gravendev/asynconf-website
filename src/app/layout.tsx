import { Anchor, AosInit, Button, Footer, Nav } from '@/components';
import type { Metadata } from 'next';
import { icons } from '../../public/icons';
import { Open_Sans } from 'next/font/google';
import './globals.css';

export const metadata: Metadata = {
	title: 'Asynconf 2023 | Salon sur le développement informatique',
	description:
		"Du 30 au 31 oct, découvrez un événement gratuit à la fois en ligne et en presentiel sur le développement informatique. ",
	keywords: ['Asynconf 2023', 'développement informatique', 'programmation', 'conférence', 'événement tech'],
};

const open = Open_Sans({ subsets: ['latin'] });

export default function RootLayout({ children }: { children: React.ReactNode }) {
	return (
		<html lang="fr" className="scroll-smooth">
			<AosInit />
			<body className={`bg-black text-white min-h-screen ${open.className}`}>
				<Nav />
				{children}
				<Footer />
				<Anchor href="#" className="fixed bottom-5 lg:bottom-10 right-5 lg:right-10 z-40">
					<Button className="h-8 w-8 p-2" icon={icons.chevronUp} />
				</Anchor>
			</body>
		</html>
	);
}
